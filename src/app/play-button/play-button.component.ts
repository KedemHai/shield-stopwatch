import {Component, HostListener, Inject} from '@angular/core';
import {ButtonView} from '../../../Stopwatch/UI/Button/ButtonContract';
import ButtonViewButtonComponent from '../../../Stopwatch/Infrastructure/Angular/ButtonViewButtonComponent';
import ToggleStopwatchButtonPresenter
  from '../../../Stopwatch/UI/ToggleStopwatchButton/ToggleStopwatchButtonPresenter';
import ToggleStopwatchButtonState
  from '../../../Stopwatch/UI/ToggleStopwatchButton/ToggleStopwatchButtonState';
import ToggleStopwatchButtonPausedState
  from '../../../Stopwatch/UI/ToggleStopwatchButton/ToggleStopwatchButtonPausedState';
import {ButtonComponent} from '../button/button.component';
import {StopwatchService} from '../stopwatch.service';
import ToggleStopwatchButtonStartedState
  from '../../../Stopwatch/UI/ToggleStopwatchButton/ToggleStopwatchButtonStartedState';
import {
  PlayStopwatchInput,
  PlayStopwatchOutput,
} from '../../../Stopwatch/Application/PlayStopwatch/PlayStopwatchContract';
import {
  PauseStopwatchInput,
  PauseStopwatchOutput,
} from '../../../Stopwatch/Application/PauseStopwatch/PauseStopwatchContract';
import {PAUSE_STOPWATCH, PLAY_STOPWATCH} from '../tokens';
import StopwatchServiceForPauseStopwatchOutput
  from '../../../Stopwatch/Infrastructure/Angular/StopwatchServiceForPauseStopwatchOutput';
import StopwatchServiceForPlayStopwatchOutput
  from '../../../Stopwatch/Infrastructure/Angular/StopwatchServiceForPlayStopwatchOutput';
import UiIcon from '../../../Stopwatch/UI/UiIcon/UiIcon';

@Component({
  selector: 'app-play-button',
  templateUrl: '../button/button.component.html',
  styleUrls: ['../button/button.component.scss'],
})
export class PlayButtonComponent extends ButtonComponent {
  private readonly playStopwatch: PlayStopwatchInput;
  private readonly pauseStopwatch: PauseStopwatchInput;
  private readonly stopwatchService: StopwatchService;
  private isStopwatchRunning: boolean = false;
  public readonly icon: UiIcon = UiIcon.PLAY;

  public constructor(
      @Inject(PLAY_STOPWATCH)playStopwatch: PlayStopwatchInput,
      @Inject(PAUSE_STOPWATCH)pauseStopwatch: PauseStopwatchInput,
      stopwatchService: StopwatchService) {
    super();
    this.playStopwatch = playStopwatch;
    this.pauseStopwatch = pauseStopwatch;
    this.stopwatchService = stopwatchService;
  }

  ngOnInit(): void {
    const view: ButtonView = new ButtonViewButtonComponent(this);
    const initialState: ToggleStopwatchButtonState = new ToggleStopwatchButtonPausedState();
    const presenter = new ToggleStopwatchButtonPresenter(view, initialState);
    view.attach(presenter);

    this.stopwatchService.observeIsStopwatchRunning().subscribe((value) => {
      this.isStopwatchRunning = value;

      let state: ToggleStopwatchButtonState;
      if (value) {
        state = new ToggleStopwatchButtonStartedState();
      } else {
        state = new ToggleStopwatchButtonPausedState();
      }

      presenter.changeState(state);
    });
    super.ngOnInit();
  }

  @HostListener('click')
  private onClick() {
    if (this.isStopwatchRunning) {
      let output: PauseStopwatchOutput;
      output = new StopwatchServiceForPauseStopwatchOutput(
          this.stopwatchService);
      return this.pauseStopwatch.execute(output);
    }
    let output: PlayStopwatchOutput;
    output = new StopwatchServiceForPlayStopwatchOutput(this.stopwatchService);
    return this.playStopwatch.execute(output);
  }
}
