import {
  Component,
  ElementRef,
  Input,
  OnChanges,
  OnInit,
  Renderer2,
  SimpleChanges,
} from '@angular/core';
import TimerModelWithLeadingZero
  from '../../../Stopwatch/UI/Timer/TimerModelWithLeadingZero';
import TimerViewSimpleTimerComponent from '../../../Stopwatch/Infrastructure/Angular/TimerViewSimpleTimerComponent';
import TimerPresenter from '../../../Stopwatch/UI/Timer/TimerPresenter';
import {
  TimerModel,
  TimerUserActions,
  TimerView,
} from '../../../Stopwatch/UI/Timer/TimerContract';

@Component({
  selector: 'app-simple-timer',
  templateUrl: './simple-timer.component.html',
  styleUrls: ['./simple-timer.component.scss'],
})
export class SimpleTimerComponent implements OnChanges, OnInit {
  private readonly renderer: Renderer2;
  private readonly elementRef: ElementRef;
  private blinkIntervalId?: number;
  @Input() public time?: number;
  @Input() public blinkInterval: number | null = null;
  public milliseconds: string = '00';
  public seconds: string = '00';
  public minutes: string = '00';

  public constructor(renderer: Renderer2, elementRef: ElementRef) {
    this.renderer = renderer;
    this.elementRef = elementRef;
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('time') && (typeof this.time !== 'undefined')) {
      const model: TimerModel = new TimerModelWithLeadingZero();
      const view: TimerView = new TimerViewSimpleTimerComponent(this);
      const presenter: TimerUserActions = new TimerPresenter(view, model,
          this.time);
      view.attach(presenter);
    }

    if (changes.hasOwnProperty('blinkInterval')) {
      this.updateBlinkInterval();
    }
  }

  ngOnInit(): void {
  }

  private updateBlinkInterval(): void {
    let isBlinking: boolean = false;
    if (this.blinkInterval === null) {
      if (typeof this.blinkIntervalId !== 'undefined') {
        clearInterval(this.blinkIntervalId);
      }

      return this.renderer.removeClass(this.elementRef.nativeElement,
          'blinking');
    }
    this.blinkIntervalId = setInterval(() => {
      isBlinking = !isBlinking;
      if (isBlinking) {
        return this.renderer.addClass(this.elementRef.nativeElement,
            'blinking');
      }
      return this.renderer.removeClass(this.elementRef.nativeElement,
          'blinking');
    }, this.blinkInterval / 2 * 1000);
  }
}
