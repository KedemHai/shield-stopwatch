import {Component, Input, OnInit} from '@angular/core';
import UiIcon from '../../../Stopwatch/UI/UiIcon/UiIcon';
import {IconProp} from '@fortawesome/fontawesome-svg-core';
import {
  faPause,
  faPlay,
  faStopwatch,
  faTrashAlt,
} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
})
export class ButtonComponent implements OnInit {
  @Input() public text?: string;
  @Input() public icon?: UiIcon;

  public get fontAwesomeProp(): IconProp {
    switch (this.icon as UiIcon) {
      case UiIcon.PLAY:
        return faPlay;
      case UiIcon.PAUSE:
        return faPause;
      case UiIcon.TIMER:
        return faStopwatch;
      case UiIcon.TRASH:
        return faTrashAlt;
      default:
        return faStopwatch;
    }
  }

  public ngOnInit(): void {
  }
}
