import {Component, Inject, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {
  UpdateStopwatchInput,
  UpdateStopwatchOutput,
  UpdateStopwatchResponse,
} from '../../Stopwatch/Application/UpdateStopwatch/UpdateStopwatchContract';
import {
  ADD_STOPWATCH_RECORD,
  PAUSE_STOPWATCH,
  PLAY_STOPWATCH,
  RESET_STOPWATCH,
  UPDATE_STOPWATCH,
} from './tokens';
import {ButtonComponent} from './button/button.component';
import {PlayStopwatchInput} from '../../Stopwatch/Application/PlayStopwatch/PlayStopwatchContract';
import {PauseStopwatchInput} from '../../Stopwatch/Application/PauseStopwatch/PauseStopwatchContract';
import {StopwatchService} from './stopwatch.service';
import {Subscription} from 'rxjs';
import UiIcon from '../../Stopwatch/UI/UiIcon/UiIcon';
import {
  AddStopwatchRecordInput,
  AddStopwatchRecordOutput,
} from '../../Stopwatch/Application/AddStopwatchRecord/AddStopwatchRecordContract';
import StopwatchRecordsServiceForAddStopwatchRecordOutput
  from '../../Stopwatch/Infrastructure/Angular/StopwatchRecordsServiceForAddStopwatchRecordOutput';
import {StopwatchRecordsService} from './stopwatch-records.service';
import {
  ResetStopwatchInput,
  ResetStopwatchOutput,
} from '../../Stopwatch/Application/ResetStopwatch/ResetStopwatchContract';
import StopwatchServiceForResetStopwatchOutput
  from '../../Stopwatch/Infrastructure/Angular/StopwatchServiceForResetStopwatchOutput';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy, UpdateStopwatchOutput {
  private readonly updateStopwatch: UpdateStopwatchInput;
  private readonly playStopwatch: PlayStopwatchInput;
  private readonly pauseStopwatch: PauseStopwatchInput;
  private readonly addStopwatchRecord: AddStopwatchRecordInput;
  private readonly resetStopwatch: ResetStopwatchInput;
  private readonly stopwatchService: StopwatchService;
  private readonly stopwatchRecordsService: StopwatchRecordsService;

  @ViewChild('toggleStopwatchButtonComponent',
      {read: ButtonComponent, static: true})
  private readonly toggleStopwatchButtonComponent?: ButtonComponent;

  @ViewChild('addRecordButtonComponent', {read: ButtonComponent, static: true})
  private readonly addRecordButtonComponent?: ButtonComponent;

  private isStopwatchRunning: boolean = false;
  private timeoutUpdate?: any;
  private isStopwatchRunningSubscription?: Subscription;
  private stopwatchResetSubscription?: Subscription;

  public stopwatchTime: number = 0;
  public timerBlinkInterval: number | null = null;
  public iconTimer: UiIcon = UiIcon.TIMER;
  public iconTrash: UiIcon = UiIcon.TRASH;

  constructor(
      @Inject(UPDATE_STOPWATCH)updateStopwatch: UpdateStopwatchInput,
      @Inject(PLAY_STOPWATCH)playStopwatch: PlayStopwatchInput,
      @Inject(PAUSE_STOPWATCH)pauseStopwatch: PauseStopwatchInput,
      @Inject(ADD_STOPWATCH_RECORD)addStopwatchRecord: AddStopwatchRecordInput,
      @Inject(RESET_STOPWATCH)resetStopwatch: ResetStopwatchInput,
      stopwatchService: StopwatchService,
      stopwatchRecordsService: StopwatchRecordsService) {
    this.updateStopwatch = updateStopwatch;
    this.playStopwatch = playStopwatch;
    this.pauseStopwatch = pauseStopwatch;
    this.addStopwatchRecord = addStopwatchRecord;
    this.resetStopwatch = resetStopwatch;
    this.stopwatchService = stopwatchService;
    this.stopwatchRecordsService = stopwatchRecordsService;
  }

  public ngOnInit(): void {
    this.initStopwatchUpdate();

    this.isStopwatchRunningSubscription = this.stopwatchService.observeIsStopwatchRunning().
        subscribe((value) => {
          this.isStopwatchRunning = value;
          this.initStopwatchUpdate();
        });

    this.stopwatchResetSubscription = this.stopwatchService.observeStopwatchReset().
        subscribe(() => {
          this.initStopwatchUpdate();
        });
  }

  public ngOnDestroy(): void {
    this.isStopwatchRunningSubscription?.unsubscribe();
    this.stopwatchResetSubscription?.unsubscribe();
  }

  public presentStopwatchFailedToUpdate(): void {
    throw new Error('Stopwatch failed to update');
  }

  public presentStopwatchUpdate(response: UpdateStopwatchResponse): void {
    this.stopwatchTime = response.time;
    this.stopwatchService.notifyStopwatchIsRunning(response.isRunning);
  }

  private initStopwatchUpdate(): void {
    this.clearStopwatch();

    const initUpdate = () => {
      this.updateStopwatch.execute(this);

      if (this.isStopwatchRunning) {
        this.timerBlinkInterval = 1;
        this.timeoutUpdate = setTimeout(initUpdate, 1);
      }
    };
    initUpdate();
  }

  private clearStopwatch(): void {
    if (typeof this.timeoutUpdate !== 'undefined') {
      clearTimeout(this.timeoutUpdate);
    }
    this.timerBlinkInterval = null;
  }

  public onAddStopwatchRecordClick() {
    let output: AddStopwatchRecordOutput;
    output = new StopwatchRecordsServiceForAddStopwatchRecordOutput(
        this.stopwatchRecordsService);
    this.addStopwatchRecord.execute(output);
  }

  public onResetStopwatchClick(): void {
    let output: ResetStopwatchOutput;
    output = new StopwatchServiceForResetStopwatchOutput(this.stopwatchService);
    this.resetStopwatch.execute(output);
  }
}
