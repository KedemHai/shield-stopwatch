import {InjectionToken} from '@angular/core';
import {UpdateStopwatchInput} from '../../Stopwatch/Application/UpdateStopwatch/UpdateStopwatchContract';
import {StopwatchesGateway} from '../../Stopwatch/Gateways/StopwatchesGateway';
import {PlayStopwatchInput} from '../../Stopwatch/Application/PlayStopwatch/PlayStopwatchContract';
import {PauseStopwatchInput} from '../../Stopwatch/Application/PauseStopwatch/PauseStopwatchContract';
import {AddStopwatchRecordInput} from '../../Stopwatch/Application/AddStopwatchRecord/AddStopwatchRecordContract';
import {StopwatchRecordsGateway} from '../../Stopwatch/Gateways/StopwatchRecordsGateway';
import {ResetStopwatchInput} from '../../Stopwatch/Application/ResetStopwatch/ResetStopwatchContract';
import {GetStopwatchRecordsInput} from '../../Stopwatch/Application/GetStopwatchRecords/GetStopwatchRecordsContract';

export const PLAY_STOPWATCH: InjectionToken<PlayStopwatchInput> = new InjectionToken<PlayStopwatchInput>(
    '');
export const PAUSE_STOPWATCH: InjectionToken<PauseStopwatchInput> = new InjectionToken<PauseStopwatchInput>(
    '');
export const UPDATE_STOPWATCH: InjectionToken<UpdateStopwatchInput> = new InjectionToken<UpdateStopwatchInput>(
    '');
export const STOPWATCHES_GATEWAY: InjectionToken<StopwatchesGateway> = new InjectionToken<StopwatchesGateway>(
    '');
export const ADD_STOPWATCH_RECORD: InjectionToken<AddStopwatchRecordInput> = new InjectionToken<AddStopwatchRecordInput>(
    '');
export const STOPWATCH_RECORDS_GATEWAY: InjectionToken<StopwatchRecordsGateway> = new InjectionToken<StopwatchRecordsGateway>(
    '');
export const RESET_STOPWATCH: InjectionToken<ResetStopwatchInput> = new InjectionToken<ResetStopwatchInput>(
    '');
export const GET_STOPWATCH_RECORDS: InjectionToken<GetStopwatchRecordsInput> = new InjectionToken<GetStopwatchRecordsInput>(
    '');
export const LOCAL_STORAGE: InjectionToken<Storage> = new InjectionToken<Storage>(
    '');
