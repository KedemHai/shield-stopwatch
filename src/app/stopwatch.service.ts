import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable, ReplaySubject, Subject} from 'rxjs';
import {distinctUntilChanged} from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class StopwatchService {
  private readonly stopwatchIsRunning: Subject<boolean> = new BehaviorSubject<boolean>(
      false);

  private readonly stopwatchReset: Subject<void> = new ReplaySubject(1);

  public observeIsStopwatchRunning(): Observable<boolean> {
    return this.stopwatchIsRunning.asObservable().pipe(distinctUntilChanged());
  }

  public notifyStopwatchIsRunning(isRunning: boolean): void {
    this.stopwatchIsRunning.next(isRunning);
  }

  public observeStopwatchReset(): Observable<void> {
    return this.stopwatchReset.asObservable();
  }

  public notifyStopwatchReset(): void {
    this.stopwatchReset.next();
    this.stopwatchIsRunning.next(false);
  }
}
