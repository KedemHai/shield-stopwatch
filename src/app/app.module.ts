import {BrowserModule} from '@angular/platform-browser';
import {Injector, NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {
  ADD_STOPWATCH_RECORD,
  GET_STOPWATCH_RECORDS,
  LOCAL_STORAGE,
  PAUSE_STOPWATCH,
  PLAY_STOPWATCH,
  RESET_STOPWATCH,
  STOPWATCH_RECORDS_GATEWAY,
  STOPWATCHES_GATEWAY,
  UPDATE_STOPWATCH,
} from './tokens';
import UpdateStopwatch
  from '../../Stopwatch/Application/UpdateStopwatch/UpdateStopwatch';
import {StopwatchesGateway} from '../../Stopwatch/Gateways/StopwatchesGateway';
import {PlayButtonComponent} from './play-button/play-button.component';
import PlayStopwatch
  from '../../Stopwatch/Application/PlayStopwatch/PlayStopwatch';
import PauseStopwatch
  from '../../Stopwatch/Application/PauseStopwatch/PauseStopwatch';
import {ButtonComponent} from './button/button.component';
import {SimpleTimerComponent} from './simple-timer/simple-timer.component';
import {StopwatchService} from './stopwatch.service';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {StopwatchRecordsService} from './stopwatch-records.service';
import AddStopwatchRecord
  from '../../Stopwatch/Application/AddStopwatchRecord/AddStopwatchRecord';
import {StopwatchRecordsGateway} from '../../Stopwatch/Gateways/StopwatchRecordsGateway';
import {StopwatchRecordsComponent} from './stopwatch-records/stopwatch-records.component';
import ResetStopwatch
  from '../../Stopwatch/Application/ResetStopwatch/ResetStopwatch';
import GetStopwatchRecords
  from '../../Stopwatch/Application/GetStopwatchRecords/GetStopwatchRecords';
import LocalStorageProxyForInMemoryStopwatchesGateway
  from '../../Stopwatch/Infrastructure/LocalStorage/LocalStorageProxyForInMemoryStopwatchesGateway';
import LocalStorageProxyForInMemoryStopwatchRecordsGateway
  from '../../Stopwatch/Infrastructure/LocalStorage/LocalStorageProxyForInMemoryStopwatchRecordsGateway';

@NgModule({
  declarations: [
    AppComponent,
    PlayButtonComponent,
    ButtonComponent,
    SimpleTimerComponent,
    StopwatchRecordsComponent,
  ],
  imports: [
    BrowserModule,
    FontAwesomeModule,
  ],
  providers: [
    {
      provide: PLAY_STOPWATCH,
      useFactory: (injector: Injector) => {
        const gateway: StopwatchesGateway = injector.get<StopwatchesGateway>(
            STOPWATCHES_GATEWAY);
        return new PlayStopwatch(gateway);
      },
      deps: [
        Injector,
      ],
    },
    {
      provide: PAUSE_STOPWATCH,
      useFactory: (injector: Injector) => {
        const gateway: StopwatchesGateway = injector.get<StopwatchesGateway>(
            STOPWATCHES_GATEWAY);
        return new PauseStopwatch(gateway);
      },
      deps: [
        Injector,
      ],
    },
    {
      provide: UPDATE_STOPWATCH,
      useFactory: (injector: Injector) => {
        const gateway: StopwatchesGateway = injector.get<StopwatchesGateway>(
            STOPWATCHES_GATEWAY);
        return new UpdateStopwatch(gateway);
      },
      deps: [
        Injector,
      ],
    },
    {
      provide: STOPWATCHES_GATEWAY,
      useFactory: (storage: Storage) => {
        return new LocalStorageProxyForInMemoryStopwatchesGateway(storage);
      },
      deps: [
        LOCAL_STORAGE,
      ],
    },
    {
      provide: LOCAL_STORAGE,
      useValue: localStorage,
    },
    StopwatchService,
    {
      provide: ADD_STOPWATCH_RECORD,
      useFactory: (injector: Injector) => {
        const stopwatchesGateway: StopwatchesGateway = injector.get(
            STOPWATCHES_GATEWAY);
        const stopwatchRecordsGateway: StopwatchRecordsGateway = injector.get(
            STOPWATCH_RECORDS_GATEWAY);
        return new AddStopwatchRecord(stopwatchesGateway,
            stopwatchRecordsGateway);
      },
      deps: [
        Injector,
      ],
    },
    {
      provide: STOPWATCH_RECORDS_GATEWAY,
      useFactory: (storage: Storage) => {
        return new LocalStorageProxyForInMemoryStopwatchRecordsGateway(storage);
      },
      deps: [
        LOCAL_STORAGE,
      ],
    },
    StopwatchRecordsService,
    {
      provide: RESET_STOPWATCH,
      useFactory: (injector: Injector) => {
        const stopwatchesGateway: StopwatchesGateway = injector.get(
            STOPWATCHES_GATEWAY);
        const stopwatchRecordsGateway: StopwatchRecordsGateway = injector.get(
            STOPWATCH_RECORDS_GATEWAY);
        return new ResetStopwatch(stopwatchesGateway, stopwatchRecordsGateway);
      },
      deps: [
        Injector,
      ],
    },
    {
      provide: GET_STOPWATCH_RECORDS,
      useFactory: (injector: Injector) => {
        const stopwatchRecordsGateway: StopwatchRecordsGateway = injector.get(
            STOPWATCH_RECORDS_GATEWAY);
        return new GetStopwatchRecords(stopwatchRecordsGateway);
      },
      deps: [
        Injector,
      ],
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
}
