import {Injectable} from '@angular/core';
import {Observable, ReplaySubject, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class StopwatchRecordsService {
  private readonly stopwatchRecordAdded: Subject<number> = new ReplaySubject<number>(
      1);

  public observeStopwatchRecordAdded(): Observable<number> {
    return this.stopwatchRecordAdded.asObservable();
  }

  public handleStopwatchRecordAdded(value: number): void {
    this.stopwatchRecordAdded.next(value);
  }
}
