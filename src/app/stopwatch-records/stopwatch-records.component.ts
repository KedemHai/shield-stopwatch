import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {StopwatchRecordsService} from '../stopwatch-records.service';
import {Subscription} from 'rxjs';
import {
  GetStopwatchRecordsInput,
  GetStopwatchRecordsOutput,
} from '../../../Stopwatch/Application/GetStopwatchRecords/GetStopwatchRecordsContract';
import {GET_STOPWATCH_RECORDS} from '../tokens';
import UpdateStopwatchRecordsComponent from './UpdateStopwatchRecordsComponent';
import {StopwatchService} from '../stopwatch.service';

@Component({
  selector: 'app-stopwatch-records',
  templateUrl: './stopwatch-records.component.html',
  styleUrls: ['./stopwatch-records.component.scss'],
})
export class StopwatchRecordsComponent implements OnInit, OnDestroy {
  private readonly getStopwatchRecords: GetStopwatchRecordsInput;
  private readonly stopwatchRecordsService: StopwatchRecordsService;
  private readonly stopwatchService: StopwatchService;

  private stopwatchRecordAddedSubscription?: Subscription;
  private stopwatchResetSubscription?: Subscription;

  public records: number[] = [];

  public constructor(
      @Inject(
          GET_STOPWATCH_RECORDS)getStopwatchRecords: GetStopwatchRecordsInput,
      stopwatchRecordsService: StopwatchRecordsService,
      stopwatchService: StopwatchService) {
    this.getStopwatchRecords = getStopwatchRecords;
    this.stopwatchRecordsService = stopwatchRecordsService;
    this.stopwatchService = stopwatchService;
  }

  public ngOnInit(): void {
    this.updateRecords();

    this.stopwatchRecordAddedSubscription = this.stopwatchRecordsService.observeStopwatchRecordAdded().
        subscribe((value) => {
          this.records.push(value);
        });

    this.stopwatchResetSubscription = this.stopwatchService.observeStopwatchReset().
        subscribe(() => {
          this.updateRecords();
        });
  }

  public ngOnDestroy(): void {
    this.stopwatchRecordAddedSubscription?.unsubscribe();
    this.stopwatchResetSubscription?.unsubscribe();
  }

  private updateRecords(): void {
    const output: GetStopwatchRecordsOutput = new UpdateStopwatchRecordsComponent(
        this);
    this.getStopwatchRecords.execute(output);
  }
}
