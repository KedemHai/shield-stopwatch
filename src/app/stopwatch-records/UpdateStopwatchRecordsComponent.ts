import {GetStopwatchRecordsOutput} from '../../../Stopwatch/Application/GetStopwatchRecords/GetStopwatchRecordsContract';
import {StopwatchRecordsComponent} from './stopwatch-records.component';

class UpdateStopwatchRecordsComponent implements GetStopwatchRecordsOutput {
  private readonly component: StopwatchRecordsComponent;

  public constructor(component: StopwatchRecordsComponent) {
    this.component = component;
  }

  public presentStopwatchRecords(records: number[]): void {
    this.component.records = records;
  }
}

export default UpdateStopwatchRecordsComponent;
