import UiIcon from '../UiIcon/UiIcon';

interface ButtonUserActions {
  loaded(): void;
}

interface ButtonView {
  attach(userActions: ButtonUserActions): void;

  setText(text: string): void;

  setIcon(icon: UiIcon): void;
}

export {ButtonUserActions, ButtonView};
