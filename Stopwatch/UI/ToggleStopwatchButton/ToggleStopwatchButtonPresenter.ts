import {ButtonUserActions, ButtonView} from '../Button/ButtonContract';
import ToggleStopwatchButtonState from './ToggleStopwatchButtonState';

class ToggleStopwatchButtonPresenter implements ButtonUserActions {
  private readonly view: ButtonView;
  private toggleState: ToggleStopwatchButtonState;

  constructor(view: ButtonView, toggleState: ToggleStopwatchButtonState) {
    this.view = view;
    this.toggleState = toggleState;
  }

  public loaded(): void {
    this.toggleState.display(this.view);
  }

  public changeState(state: ToggleStopwatchButtonState): void {
    this.toggleState = state;
    this.toggleState.display(this.view);
  }
}

export default ToggleStopwatchButtonPresenter;
