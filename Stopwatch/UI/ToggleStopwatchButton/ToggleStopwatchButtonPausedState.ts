import ToggleStopwatchButtonState from './ToggleStopwatchButtonState';
import UiIcon from '../UiIcon/UiIcon';
import {ButtonView} from '../Button/ButtonContract';

class ToggleStopwatchButtonPausedState extends ToggleStopwatchButtonState {
  public display(view: ButtonView): void {
    view.setText('Play');
    view.setIcon(UiIcon.PLAY);
  }
}

export default ToggleStopwatchButtonPausedState;
