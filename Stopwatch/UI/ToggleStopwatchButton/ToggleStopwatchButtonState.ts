import {ButtonView} from '../Button/ButtonContract';

abstract class ToggleStopwatchButtonState {
  public abstract display(view: ButtonView): void;
}

export default ToggleStopwatchButtonState;
