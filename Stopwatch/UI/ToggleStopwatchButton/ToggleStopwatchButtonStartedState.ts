import ToggleStopwatchButtonState from './ToggleStopwatchButtonState';
import {ButtonView} from '../Button/ButtonContract';
import UiIcon from '../UiIcon/UiIcon';

class ToggleStopwatchButtonStartedState extends ToggleStopwatchButtonState {
  public display(view: ButtonView): void {
    view.setText('Pause');
    view.setIcon(UiIcon.PAUSE);
  }
}

export default ToggleStopwatchButtonStartedState;
