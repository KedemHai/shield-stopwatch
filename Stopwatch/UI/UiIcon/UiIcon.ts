enum UiIcon {
  PLAY,
  PAUSE,
  TIMER,
  TRASH,
}

export default UiIcon;
