import {TimerModel} from './TimerContract';

function generateLeadingZero(n: number): string {
  if (n < 10) {
    return `0${n}`;
  }

  return `${n}`;
}

class TimerModelWithLeadingZero implements TimerModel {
  private time: number = 0;

  public getMilliseconds(): string {
    const d = new Date(this.time);
    const cent = Math.floor(d.getUTCMilliseconds() / 10);
    return generateLeadingZero(cent);
  }

  public getMinutes(): string {
    const d = new Date(this.time);
    return generateLeadingZero(d.getUTCMinutes());
  }

  public getSeconds(): string {
    const d = new Date(this.time);
    return generateLeadingZero(d.getUTCSeconds());
  }

  public setTime(time: number): void {
    this.time = time;
  }
}

export default TimerModelWithLeadingZero;
