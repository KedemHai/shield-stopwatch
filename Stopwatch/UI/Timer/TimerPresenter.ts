import {TimerModel, TimerUserActions, TimerView} from './TimerContract';

class TimerPresenter implements TimerUserActions {
  private readonly view: TimerView;
  private readonly model: TimerModel;
  private readonly time: number;

  public constructor(view: TimerView, model: TimerModel, time: number) {
    this.view = view;
    this.model = model;
    this.time = time;
  }

  public loaded(): void {
    this.model.setTime(this.time);
    this.view.setMilliseconds(this.model.getMilliseconds());
    this.view.setSeconds(this.model.getSeconds());
    this.view.setMinutes(this.model.getMinutes());
  }
}

export default TimerPresenter;
