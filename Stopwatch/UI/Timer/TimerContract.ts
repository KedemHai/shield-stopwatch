interface TimerModel {
  setTime(time: number): void;

  getMinutes(): string;

  getSeconds(): string;

  getMilliseconds(): string;
}

interface TimerUserActions {
  loaded(): void;
}

interface TimerView {
  attach(userActions: TimerUserActions): void;

  setMinutes(minutes: string): void;

  setSeconds(seconds: string): void;

  setMilliseconds(milliseconds: string): void;
}

export {TimerModel, TimerUserActions, TimerView};
