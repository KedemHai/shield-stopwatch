import {Stopwatch} from '../../Domain/Stopwatch';

class InMemoryStopwatch implements Stopwatch {
  public startTime: Date | null;
  public stopTime: Date | null;
  public pauseOffset: number;

  public constructor(
      startTime: Date | null = null, stopTime: Date | null = null,
      pauseOffset: number = 0) {
    this.startTime = startTime;
    this.stopTime = stopTime;
    this.pauseOffset = pauseOffset;
  }

  public async getTime(): Promise<number> {
    if (this.startTime === null) {
      return 0;
    }
    if (this.stopTime !== null) {
      return this.stopTime.getTime() - this.startTime.getTime() -
          this.pauseOffset;
    }
    const theNow = new Date();
    return theNow.getTime() - this.startTime.getTime() - this.pauseOffset;
  }

  public async isRunning(): Promise<boolean> {
    return (
        this.startTime !== null &&
        this.stopTime === null
    );
  }

  public async pause(): Promise<void> {
    this.stopTime = new Date();
  }

  public async play(): Promise<void> {
    const theNow = new Date();
    this.calculatePauseOffset(theNow);
    this.stopTime = null;
    if (this.startTime === null) {
      this.startTime = theNow;
    }
  }

  public async reset(): Promise<void> {
    this.startTime = null;
    this.stopTime = null;
    this.pauseOffset = 0;
  }

  private calculatePauseOffset(theNow: Date): void {
    if (this.stopTime !== null) {
      this.pauseOffset += (theNow.getTime() - this.stopTime.getTime());
    }
  }
}

export default InMemoryStopwatch;
