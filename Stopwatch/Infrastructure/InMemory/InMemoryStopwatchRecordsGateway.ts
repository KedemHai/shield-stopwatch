import {StopwatchRecordsGateway} from '../../Gateways/StopwatchRecordsGateway';

class InMemoryStopwatchRecordsGateway implements StopwatchRecordsGateway {
  private records: number[];

  public constructor(records: number[] = []) {
    this.records = records;
  }

  public async add(record: number): Promise<void> {
    this.records.push(record);
  }

  public async clear(): Promise<void> {
    this.records = [];
  }

  public async getAll(): Promise<number[]> {
    return [...this.records];
  }
}

export default InMemoryStopwatchRecordsGateway;
