import {StopwatchesGateway} from '../../Gateways/StopwatchesGateway';
import InMemoryStopwatch from './InMemoryStopwatch';

class InMemoryStopwatchesGateway implements StopwatchesGateway {
  protected current: InMemoryStopwatch = new InMemoryStopwatch();

  public async add(stopwatch: InMemoryStopwatch): Promise<void> {
    this.current = stopwatch;
  }

  public async getCurrent(): Promise<InMemoryStopwatch> {
    return this.current;
  }
}

export default InMemoryStopwatchesGateway;
