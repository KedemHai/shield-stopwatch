import {StopwatchService} from '../../../src/app/stopwatch.service';
import {PauseStopwatchOutput} from '../../Application/PauseStopwatch/PauseStopwatchContract';

class StopwatchServiceForPauseStopwatchOutput implements PauseStopwatchOutput {
  private readonly stopwatchService: StopwatchService;

  public constructor(stopwatchService: StopwatchService) {
    this.stopwatchService = stopwatchService;
  }

  public presentStopwatchFailedToPause(): void {
    throw new Error('Stopwatch failed to Pause')
  }

  public presentStopwatchHasPaused(): void {
    this.stopwatchService.notifyStopwatchIsRunning(false);
  }
}

export default StopwatchServiceForPauseStopwatchOutput;
