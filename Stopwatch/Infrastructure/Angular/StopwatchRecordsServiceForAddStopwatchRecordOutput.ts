import {AddStopwatchRecordOutput} from '../../Application/AddStopwatchRecord/AddStopwatchRecordContract';
import {StopwatchRecordsService} from '../../../src/app/stopwatch-records.service';

class StopwatchRecordsServiceForAddStopwatchRecordOutput implements AddStopwatchRecordOutput {
  private readonly stopwatchRecordsService: StopwatchRecordsService;

  public constructor(stopwatchRecordsService: StopwatchRecordsService) {
    this.stopwatchRecordsService = stopwatchRecordsService;
  }

  public presentFailedToAddStopwatchRecord(): void {
    throw new Error('Failed to add Stopwatch Record');
  }

  public presentStopwatchRecordAdded(time: number): void {
    this.stopwatchRecordsService.handleStopwatchRecordAdded(time);
  }
}

export default StopwatchRecordsServiceForAddStopwatchRecordOutput;
