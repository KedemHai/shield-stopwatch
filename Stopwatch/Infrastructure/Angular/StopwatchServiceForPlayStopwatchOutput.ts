import {PlayStopwatchOutput} from '../../Application/PlayStopwatch/PlayStopwatchContract';
import {StopwatchService} from '../../../src/app/stopwatch.service';

class StopwatchServiceForPlayStopwatchOutput implements PlayStopwatchOutput {
  private readonly stopwatchService: StopwatchService;

  public constructor(stopwatchService: StopwatchService) {
    this.stopwatchService = stopwatchService;
  }

  public presentStopwatchFailedTotPlay(): void {
    throw new Error('Stopwatch failed to Play')
  }

  public presentStopwatchHasStarted(): void {
    this.stopwatchService.notifyStopwatchIsRunning(true);
  }
}

export default StopwatchServiceForPlayStopwatchOutput;
