import {
  TimerUserActions,
  TimerView,
} from '../../UI/Timer/TimerContract';
import {SimpleTimerComponent} from '../../../src/app/simple-timer/simple-timer.component';

class TimerViewSimpleTimerComponent implements TimerView {
  private readonly component: SimpleTimerComponent;

  constructor(component: SimpleTimerComponent) {
    this.component = component;
  }

  attach(userActions: TimerUserActions): void {
    userActions.loaded();
  }

  setMilliseconds(milliseconds: string): void {
    this.component.milliseconds = milliseconds;
  }

  setMinutes(minutes: string): void {
    this.component.minutes = minutes;
  }

  setSeconds(seconds: string): void {
    this.component.seconds = seconds;
  }
}

export default TimerViewSimpleTimerComponent;
