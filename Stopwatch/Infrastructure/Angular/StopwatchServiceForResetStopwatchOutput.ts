import {ResetStopwatchOutput} from '../../Application/ResetStopwatch/ResetStopwatchContract';
import {StopwatchService} from '../../../src/app/stopwatch.service';

class StopwatchServiceForResetStopwatchOutput implements ResetStopwatchOutput {
  private readonly stopwatchService: StopwatchService;

  public constructor(stopwatchService: StopwatchService) {
    this.stopwatchService = stopwatchService;
  }

  presentFailedToResetStopwatch(): void {
    throw new Error('Failed to reset Stopwatch');
  }

  presentResetStopwatchSuccess(): void {
    this.stopwatchService.notifyStopwatchReset();
  }
}

export default StopwatchServiceForResetStopwatchOutput;
