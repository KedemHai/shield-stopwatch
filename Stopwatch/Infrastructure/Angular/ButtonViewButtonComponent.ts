import {
  ButtonUserActions,
  ButtonView,
} from '../../UI/Button/ButtonContract';
import {ButtonComponent} from '../../../src/app/button/button.component';
import UiIcon from '../../UI/UiIcon/UiIcon';

class ButtonViewButtonComponent implements ButtonView {
  private readonly component: ButtonComponent;

  public constructor(component: ButtonComponent) {
    this.component = component;
  }

  attach(userActions: ButtonUserActions): void {
    userActions.loaded();
  }

  setIcon(icon: UiIcon): void {
    this.component.icon = icon;
  }

  setText(text: string): void {
    this.component.text = text;
  }
}

export default ButtonViewButtonComponent;
