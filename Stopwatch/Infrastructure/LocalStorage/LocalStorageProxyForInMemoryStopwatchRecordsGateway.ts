import InMemoryStopwatchRecordsGateway
  from '../InMemory/InMemoryStopwatchRecordsGateway';

class LocalStorageProxyForInMemoryStopwatchRecordsGateway extends InMemoryStopwatchRecordsGateway {
  private static localStorageKey: string = 'shield-stopwatch-records';
  private readonly storage: Storage;

  public constructor(storage: Storage) {
    const item = storage.getItem(
        LocalStorageProxyForInMemoryStopwatchRecordsGateway.localStorageKey);
    let records: number[] = [];
    if (item !== null) {
      records = JSON.parse(item);
    }
    super(records);
    this.storage = storage;
  }

  public async clear(): Promise<void> {
    this.storage.removeItem(
        LocalStorageProxyForInMemoryStopwatchRecordsGateway.localStorageKey);
    return super.clear();
  }

  public async add(record: number): Promise<void> {
    const records = this.getRecordsFromItem();
    records.push(record);
    this.storage.setItem(
        LocalStorageProxyForInMemoryStopwatchRecordsGateway.localStorageKey,
        JSON.stringify(records));
    return super.add(record);
  }

  private getRecordsFromItem(): number[] {
    const item = this.storage.getItem(
        LocalStorageProxyForInMemoryStopwatchRecordsGateway.localStorageKey);
    let records: number[] = [];
    if (item !== null) {
      records = JSON.parse(item);
    }
    return records;
  }
}

export default LocalStorageProxyForInMemoryStopwatchRecordsGateway;
