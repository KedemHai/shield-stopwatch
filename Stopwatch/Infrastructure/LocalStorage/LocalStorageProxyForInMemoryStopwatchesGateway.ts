import InMemoryStopwatchesGateway from '../InMemory/InMemoryStopwatchesGateway';
import InMemoryStopwatch from '../InMemory/InMemoryStopwatch';

interface LocalStorageStopwatch {
  startTime: Date | null;
  stopTime: Date | null;
  pauseOffset: number;
}

function getDateOrNull(strDate: Date | null): Date | null {
  if (strDate !== null) {
    return new Date(strDate);
  }

  return strDate;
}

class LocalStorageProxyForInMemoryStopwatchesGateway extends InMemoryStopwatchesGateway {
  private readonly storage: Storage;

  public constructor(storage: Storage) {
    super();

    this.storage = storage;
    let stopwatch: InMemoryStopwatch;
    const data = storage.getItem('shield-stopwatch');
    if (data === null) {
      stopwatch = new InMemoryStopwatch();
    } else {
      const item = JSON.parse(data) as LocalStorageStopwatch;
      stopwatch = new InMemoryStopwatch(getDateOrNull(item.startTime),
          getDateOrNull(item.stopTime), item.pauseOffset);
    }
    this.current = stopwatch;
  }

  public async add(stopwatch: InMemoryStopwatch): Promise<void> {
    const data = {
      startTime: stopwatch.startTime,
      stopTime: stopwatch.stopTime,
      pauseOffset: stopwatch.pauseOffset,
    } as LocalStorageStopwatch;
    this.storage.setItem('shield-stopwatch', JSON.stringify(data));

    return super.add(stopwatch);
  }
}

export default LocalStorageProxyForInMemoryStopwatchesGateway;
