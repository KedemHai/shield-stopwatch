interface StopwatchRecordsGateway {
  add(record: number): Promise<void>;

  clear(): Promise<void>;

  getAll(): Promise<number[]>
}

export {StopwatchRecordsGateway};
