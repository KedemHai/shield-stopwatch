import {Stopwatch} from '../Domain/Stopwatch';

interface StopwatchesGateway {
  getCurrent(): Promise<Stopwatch>;

  add(stopwatch: Stopwatch): Promise<void>;
}

export {StopwatchesGateway};
