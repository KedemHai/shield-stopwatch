import {
  GetStopwatchRecordsInput,
  GetStopwatchRecordsOutput,
} from './GetStopwatchRecordsContract';
import {StopwatchRecordsGateway} from '../../Gateways/StopwatchRecordsGateway';

class GetStopwatchRecords implements GetStopwatchRecordsInput {
  private readonly stopwatchRecordsGateway: StopwatchRecordsGateway;

  public constructor(stopwatchRecordsGateway: StopwatchRecordsGateway) {
    this.stopwatchRecordsGateway = stopwatchRecordsGateway;
  }

  public async execute(output: GetStopwatchRecordsOutput): Promise<void> {
    const records: number[] = await this.stopwatchRecordsGateway.getAll();
    output.presentStopwatchRecords([...records]);
  }
}

export default GetStopwatchRecords;
