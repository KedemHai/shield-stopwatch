interface GetStopwatchRecordsInput {
  execute(output: GetStopwatchRecordsOutput): void;
}

interface GetStopwatchRecordsOutput {
  presentStopwatchRecords(records: number[]): void;
}

export {GetStopwatchRecordsInput, GetStopwatchRecordsOutput};
