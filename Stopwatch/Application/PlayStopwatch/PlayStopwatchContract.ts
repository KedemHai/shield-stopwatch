interface PlayStopwatchInput {
  execute(output: PlayStopwatchOutput): void;
}

interface PlayStopwatchOutput {
  presentStopwatchHasStarted(): void;

  presentStopwatchFailedTotPlay(): void;
}

export {PlayStopwatchInput, PlayStopwatchOutput};
