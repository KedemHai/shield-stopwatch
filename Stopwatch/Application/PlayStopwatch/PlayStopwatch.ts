import {PlayStopwatchInput, PlayStopwatchOutput} from './PlayStopwatchContract';
import {StopwatchesGateway} from '../../Gateways/StopwatchesGateway';
import {Stopwatch} from '../../Domain/Stopwatch';

class PlayStopwatch implements PlayStopwatchInput {
  private readonly stopwatchesGateway: StopwatchesGateway;

  public constructor(stopwatchesGateway: StopwatchesGateway) {
    this.stopwatchesGateway = stopwatchesGateway;
  }

  public async execute(output: PlayStopwatchOutput): Promise<void> {
    try {
      const stopwatch: Stopwatch = await this.stopwatchesGateway.getCurrent();
      await stopwatch.play();
      await this.stopwatchesGateway.add(stopwatch);
      output.presentStopwatchHasStarted();
    } catch (e) {
      output.presentStopwatchFailedTotPlay();
    }
  }
}

export default PlayStopwatch;
