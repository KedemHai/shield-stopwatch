import {
  AddStopwatchRecordInput,
  AddStopwatchRecordOutput,
} from './AddStopwatchRecordContract';
import {StopwatchesGateway} from '../../Gateways/StopwatchesGateway';
import {Stopwatch} from '../../Domain/Stopwatch';
import {StopwatchRecordsGateway} from '../../Gateways/StopwatchRecordsGateway';

class AddStopwatchRecord implements AddStopwatchRecordInput {
  private readonly stopwatchesGateway: StopwatchesGateway;
  private readonly stopwatchRecordsGateway: StopwatchRecordsGateway;

  public constructor(
      stopwatchesGateway: StopwatchesGateway,
      stopwatchRecordsGateway: StopwatchRecordsGateway) {
    this.stopwatchesGateway = stopwatchesGateway;
    this.stopwatchRecordsGateway = stopwatchRecordsGateway;
  }

  public async execute(output: AddStopwatchRecordOutput): Promise<void> {
    try {
      const stopwatch: Stopwatch = await this.stopwatchesGateway.getCurrent();
      const theTime: number = await stopwatch.getTime();
      await this.stopwatchRecordsGateway.add(theTime);
      output.presentStopwatchRecordAdded(theTime);
    } catch (e) {
      output.presentFailedToAddStopwatchRecord();
    }
  }
}

export default AddStopwatchRecord;
