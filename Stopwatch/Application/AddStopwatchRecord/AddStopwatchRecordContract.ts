interface AddStopwatchRecordInput {
  execute(output: AddStopwatchRecordOutput): void;
}

interface AddStopwatchRecordOutput {
  presentStopwatchRecordAdded(time: number): void;

  presentFailedToAddStopwatchRecord(): void;
}

export {AddStopwatchRecordInput, AddStopwatchRecordOutput};
