import {
  ResetStopwatchInput,
  ResetStopwatchOutput,
} from './ResetStopwatchContract';
import {StopwatchesGateway} from '../../Gateways/StopwatchesGateway';
import {StopwatchRecordsGateway} from '../../Gateways/StopwatchRecordsGateway';
import {Stopwatch} from '../../Domain/Stopwatch';

class ResetStopwatch implements ResetStopwatchInput {
  private readonly stopwatchesGateway: StopwatchesGateway;
  private readonly stopwatchRecordsGateway: StopwatchRecordsGateway;

  public constructor(
      stopwatchesGateway: StopwatchesGateway,
      stopwatchRecordsGateway: StopwatchRecordsGateway) {
    this.stopwatchesGateway = stopwatchesGateway;
    this.stopwatchRecordsGateway = stopwatchRecordsGateway;
  }

  public async execute(output: ResetStopwatchOutput): Promise<void> {
    try {
      const stopwatch: Stopwatch = await this.stopwatchesGateway.getCurrent();
      await stopwatch.reset();
      await this.stopwatchesGateway.add(stopwatch);
      await this.stopwatchRecordsGateway.clear();
      output.presentResetStopwatchSuccess();
    } catch (e) {
      output.presentFailedToResetStopwatch();
    }
  }
}

export default ResetStopwatch;
