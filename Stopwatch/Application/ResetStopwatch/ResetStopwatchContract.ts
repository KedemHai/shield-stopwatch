interface ResetStopwatchInput {
  execute(output: ResetStopwatchOutput): void;
}

interface ResetStopwatchOutput {
  presentResetStopwatchSuccess(): void;

  presentFailedToResetStopwatch(): void;
}

export {ResetStopwatchInput, ResetStopwatchOutput};
