interface PauseStopwatchInput {
  execute(output: PauseStopwatchOutput): void;
}

interface PauseStopwatchOutput {
  presentStopwatchHasPaused(): void;

  presentStopwatchFailedToPause(): void;
}

export {PauseStopwatchInput, PauseStopwatchOutput};
