import {
  PauseStopwatchInput,
  PauseStopwatchOutput,
} from './PauseStopwatchContract';
import {StopwatchesGateway} from '../../Gateways/StopwatchesGateway';
import {Stopwatch} from '../../Domain/Stopwatch';

class PauseStopwatch implements PauseStopwatchInput {
  private readonly stopwatchesGateway: StopwatchesGateway;

  public constructor(stopwatchesGateway: StopwatchesGateway) {
    this.stopwatchesGateway = stopwatchesGateway;
  }

  public async execute(output: PauseStopwatchOutput): Promise<void> {
    try {
      const stopwatch: Stopwatch = await this.stopwatchesGateway.getCurrent();
      await stopwatch.pause();
      await this.stopwatchesGateway.add(stopwatch);
      output.presentStopwatchHasPaused();
    } catch (e) {
      output.presentStopwatchFailedToPause();
    }
  }
}

export default PauseStopwatch;
