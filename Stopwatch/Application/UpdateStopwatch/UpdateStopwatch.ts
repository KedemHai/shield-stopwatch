import {
  UpdateStopwatchInput,
  UpdateStopwatchOutput,
} from './UpdateStopwatchContract';
import {StopwatchesGateway} from '../../Gateways/StopwatchesGateway';
import {Stopwatch} from '../../Domain/Stopwatch';

class UpdateStopwatch implements UpdateStopwatchInput {
  private readonly stopwatchesGateway: StopwatchesGateway;

  public constructor(stopwatchesGateway: StopwatchesGateway) {
    this.stopwatchesGateway = stopwatchesGateway;
  }

  public async execute(output: UpdateStopwatchOutput): Promise<void> {
    try {
      const stopwatch: Stopwatch = await this.stopwatchesGateway.getCurrent();
      const theTime: number = await stopwatch.getTime();
      const isRunning: boolean = await stopwatch.isRunning();
      output.presentStopwatchUpdate({
        time: theTime,
        isRunning: isRunning,
      });
    } catch (e) {
      output.presentStopwatchFailedToUpdate();
    }
  }
}

export default UpdateStopwatch;
