interface UpdateStopwatchInput {
  execute(output: UpdateStopwatchOutput): void
}

interface UpdateStopwatchOutput {
  presentStopwatchUpdate(response: UpdateStopwatchResponse): void;

  presentStopwatchFailedToUpdate(): void;
}

interface UpdateStopwatchResponse {
  time: number;
  isRunning: boolean;
}

export {UpdateStopwatchInput, UpdateStopwatchOutput, UpdateStopwatchResponse};
