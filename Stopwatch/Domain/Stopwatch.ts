interface Stopwatch {
  play(): Promise<void>;

  pause(): Promise<void>;

  getTime(): Promise<number>;

  reset(): Promise<void>;

  isRunning(): Promise<boolean>;
}

export {Stopwatch};
